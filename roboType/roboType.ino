//librairie du servo
#include <Servo.h>

// setup servo
int servoPin = 13;
int PEN_DOWN = 100;     // angle du servo Moteur pour pen down
int PEN_UP = 116;        // angle du servo Moteur pour pen up
Servo penServo;         // déclaration Servo

float wheel_dia = 65;   //     # mm (increase = spiral out) diametre des roues
float wheel_base = 113; //     # mm (increase = spiral in, ccw) diametre de l'axe des roues
int steps_rev = 512;    //     # 512 for 64x gearbox, 128 for 16x gearbox
int delay_time = 2;     //     # time between steps in ms

// Organisation des fils du moteur pas à pas org->pink->blue->yel
int L_stepper_pins[] = {12, 10, 9, 11};
int R_stepper_pins[] = {5, 3, 2, 4};

//ordre d'activation des bobine pour marche avant (.?)
int fwd_mask[][4] = {
  {1, 0, 1, 0},
  {0, 1, 1, 0},
  {0, 1, 0, 1},
  {1, 0, 0, 1}
};

int rev_mask[][4] =  {
  {1, 0, 0, 1},
  {0, 1, 0, 1},
  {0, 1, 1, 0},
  {1, 0, 1, 0}
};


void setup() {
  randomSeed(analogRead(1));
  Serial.begin(9600);
  //moteur pas à pas eteint
  for (int pin = 0; pin < 4; pin++) {
    pinMode(L_stepper_pins[pin], OUTPUT);
    digitalWrite(L_stepper_pins[pin], LOW);
    pinMode(R_stepper_pins[pin], OUTPUT);
    digitalWrite(R_stepper_pins[pin], LOW);
  }

  penServo.attach(servoPin);
  Serial.println("setup");
  penup();
  delay(1000);
}


void loop() {
  delay(250);
  dessin();

  done();      // eteindre moteur pas à pas
  while (1);   // attendre le reset
}


// ----- FUNCTIONS -----------

// transforme la distance en nombres de pas//
int step(float distance) {

  int steps = distance * steps_rev / (wheel_dia * 3.1412); //24.61
  return steps;

}


void forward(float distance) {
  //nombre de pas
  int steps = step(distance);

  Serial.println(steps);
  //avance le moteir du nombre de pas souhaités
  for (int step = 0; step < steps; step++) {
    for (int mask = 0; mask < 4; mask++) {
      for (int pin = 0; pin < 4; pin++) {
        digitalWrite(L_stepper_pins[pin], rev_mask[mask][pin]);
        digitalWrite(R_stepper_pins[pin], fwd_mask[mask][pin]);
      }
      //delais de transmission des valeurs
      delay(delay_time);
    }
  }

}

void backward(float distance) {
  int steps = step(distance);
  for (int step = 0; step < steps; step++) {
    for (int mask = 0; mask < 4; mask++) {
      for (int pin = 0; pin < 4; pin++) {
        digitalWrite(L_stepper_pins[pin], fwd_mask[mask][pin]);
        digitalWrite(R_stepper_pins[pin], rev_mask[mask][pin]);
      }
      delay(delay_time);
    }
  }
}


void right(float degrees) {
  float rotation = degrees / 360.0;
  float distance = wheel_base * 3.1412 * rotation;
  int steps = step(distance);
  for (int step = 0; step < steps; step++) {
    for (int mask = 0; mask < 4; mask++) {
      for (int pin = 0; pin < 4; pin++) {
        digitalWrite(R_stepper_pins[pin], rev_mask[mask][pin]);
        digitalWrite(L_stepper_pins[pin], rev_mask[mask][pin]);
      }
      delay(delay_time);
    }
  }
}


void left(float degrees) {
  float rotation = degrees / 360.0;
  float distance = wheel_base * 3.1412 * rotation;
  int steps = step(distance);
  for (int step = 0; step < steps; step++) {
    for (int mask = 0; mask < 4; mask++) {
      for (int pin = 0; pin < 4; pin++) {
        digitalWrite(R_stepper_pins[pin], fwd_mask[mask][pin]);
        digitalWrite(L_stepper_pins[pin], fwd_mask[mask][pin]);
      }
      delay(delay_time);
    }
  }
}


void done() {
  // éteint Mot p/p -> sauve des piles
  for (int mask = 0; mask < 4; mask++) {
    for (int pin = 0; pin < 4; pin++) {
      digitalWrite(R_stepper_pins[pin], LOW);
      digitalWrite(L_stepper_pins[pin], LOW);
    }
    delay(delay_time);
  }
}


void penup() {
  delay(250);
  Serial.println("PEN_UP()");
  penServo.write(PEN_UP);
  delay(250);
}


void pendown() {
  delay(250);
  Serial.println("PEN_DOWN()");
  penServo.write(PEN_DOWN);
  delay(250);
}

void dessin(){
penup();left(55);penup();
forward(535);
penup();left(109);pendown();
forward(4);
right(1);pendown();
forward(4);
right(0);pendown();
forward(4);
right(0);pendown();
forward(4);
left(1);pendown();
forward(5);
left(1);pendown();
forward(4);
left(1);pendown();
forward(4);
left(2);pendown();
forward(4);
left(1);pendown();
forward(4);
left(3);pendown();
forward(4);
left(2);pendown();
forward(4);
left(3);pendown();
forward(4);
left(2);pendown();
forward(4);
left(4);pendown();
forward(4);
left(3);pendown();
forward(4);
left(5);pendown();
forward(5);
right(9);pendown();
forward(4);
right(3);pendown();
forward(4);
right(2);pendown();
forward(4);
right(2);pendown();
forward(5);
right(1);pendown();
forward(5);
right(0);pendown();
forward(5);
right(1);pendown();
forward(5);
left(1);pendown();
forward(5);
left(2);pendown();
forward(4);
left(1);pendown();
forward(4);
left(3);pendown();
forward(4);
left(3);pendown();
forward(4);
left(3);pendown();
forward(4);
left(5);pendown();
forward(4);
left(4);pendown();
forward(2);
left(2);pendown();
forward(2);
left(3);pendown();
forward(2);
left(2);pendown();
forward(2);
left(8);pendown();
forward(4);
left(4);pendown();
forward(4);
left(4);pendown();
forward(2);
left(2);pendown();
forward(2);
left(3);pendown();
forward(2);
left(2);pendown();
forward(2);
left(4);pendown();
forward(2);
left(3);pendown();
forward(2);
left(3);pendown();
forward(2);
left(4);pendown();
forward(2);
left(4);pendown();
forward(2);
left(4);pendown();
forward(2);
left(4);pendown();
forward(2);
left(5);pendown();
forward(2);
penup();left(18);pendown();
forward(2);
left(8);pendown();
forward(2);
left(10);pendown();
forward(2);
left(8);pendown();
forward(2);
left(8);pendown();
forward(2);
left(7);pendown();
forward(2);
left(6);pendown();
forward(2);
left(6);pendown();
forward(2);
left(4);pendown();
forward(2);
left(4);pendown();
forward(2);
left(3);pendown();
forward(2);
left(3);pendown();
forward(2);
left(2);pendown();
forward(4);
left(3);pendown();
forward(4);
left(2);pendown();
forward(3);
left(2);pendown();
forward(3);
left(3);pendown();
forward(3);
left(2);pendown();
forward(3);
left(3);pendown();
forward(7);
left(3);pendown();
forward(7);
left(2);pendown();
forward(7);
right(0);pendown();
forward(7);
right(0);pendown();
forward(7);
right(2);pendown();
forward(7);
left(1);pendown();
forward(11);
left(2);pendown();
forward(11);
left(1);pendown();
forward(5);
left(1);pendown();
forward(5);
left(1);pendown();
forward(5);
left(2);pendown();
forward(5);
left(2);pendown();
forward(5);
left(1);pendown();
forward(5);
left(3);pendown();
forward(5);
left(1);pendown();
forward(5);
left(3);pendown();
forward(5);
left(2);pendown();
forward(5);
left(3);pendown();
forward(5);
left(3);pendown();
forward(5);
left(10);pendown();
forward(2);
left(7);pendown();
forward(3);
left(2);pendown();
forward(1);
left(1);pendown();
forward(1);
right(0);pendown();
forward(1);
right(2);pendown();
forward(1);
right(3);pendown();
forward(1);
right(7);pendown();
forward(1);
penup();right(17);pendown();
forward(0);
right(10);pendown();
forward(0);
penup();right(35);pendown();
forward(0);
penup();right(29);pendown();
forward(0);
penup();right(27);pendown();
forward(1);
penup();right(15);pendown();
forward(1);
right(6);pendown();
forward(2);
right(3);pendown();
forward(2);
left(4);pendown();
forward(8);
left(2);pendown();
forward(8);
left(1);pendown();
forward(8);
left(1);pendown();
forward(8);
left(1);pendown();
forward(16);
left(1);pendown();
forward(16);
left(1);pendown();
forward(8);
left(0);pendown();
forward(8);
left(0);pendown();
forward(8);
right(1);pendown();
forward(8);
left(0);pendown();
forward(8);
left(0);pendown();
forward(8);
right(1);pendown();
forward(8);
right(1);pendown();
forward(8);
penup();left(20);pendown();
forward(3);
right(3);pendown();
forward(3);
right(4);pendown();
forward(3);
right(3);pendown();
forward(3);
right(3);pendown();
forward(3);
right(5);pendown();
forward(3);
right(3);pendown();
forward(3);
right(5);pendown();
forward(3);
right(4);pendown();
forward(3);
right(5);pendown();
forward(3);
right(6);pendown();
forward(3);
right(6);pendown();
forward(3);
right(5);pendown();
forward(1);
right(3);pendown();
forward(1);
right(4);pendown();
forward(1);
right(4);pendown();
forward(1);
right(3);pendown();
forward(1);
right(4);pendown();
forward(2);
right(4);pendown();
forward(2);
right(4);pendown();
forward(2);
right(5);pendown();
forward(2);
right(7);pendown();
forward(2);
right(5);pendown();
forward(2);
right(6);pendown();
forward(2);
right(6);pendown();
forward(4);
right(5);pendown();
forward(4);
right(1);pendown();
forward(4);
left(2);pendown();
forward(4);
left(3);pendown();
forward(2);
left(4);pendown();
forward(2);
left(5);pendown();
forward(2);
left(5);pendown();
forward(2);
right(7);pendown();
forward(22);
left(0);pendown();
forward(22);
right(0);pendown();
forward(22);
right(0);pendown();
forward(22);
penup();right(91);pendown();
forward(3);
left(1);pendown();
forward(3);
left(2);pendown();
forward(3);
left(1);pendown();
forward(3);
left(3);pendown();
forward(6);
left(2);pendown();
forward(6);
left(1);pendown();
forward(6);
left(2);pendown();
forward(7);
left(1);pendown();
forward(7);
right(0);pendown();
forward(7);
right(0);pendown();
forward(7);
right(0);pendown();
forward(7);
right(1);pendown();
forward(6);
right(1);pendown();
forward(7);
right(2);pendown();
forward(6);
right(2);pendown();
forward(6);
right(2);pendown();
forward(3);
right(2);pendown();
forward(3);
right(2);pendown();
forward(3);
right(1);pendown();
forward(3);
penup();left(11);pendown();
forward(3);
right(3);pendown();
forward(3);
right(2);pendown();
forward(3);
right(2);pendown();
forward(3);
right(1);pendown();
forward(6);
right(2);pendown();
forward(6);
left(1);pendown();
forward(6);
left(1);pendown();
forward(6);
left(3);pendown();
forward(3);
left(1);pendown();
forward(3);
left(3);pendown();
forward(3);
left(2);pendown();
forward(3);
penup();left(50);pendown();
forward(1);
left(6);pendown();
forward(1);
left(6);pendown();
forward(1);
left(6);pendown();
forward(1);
left(6);pendown();
forward(1);
left(6);pendown();
forward(1);
left(4);pendown();
forward(1);
left(5);pendown();
forward(1);
left(6);pendown();
forward(2);
left(7);pendown();
forward(3);
left(6);pendown();
forward(3);
left(5);pendown();
forward(3);
left(4);pendown();
forward(3);
left(4);pendown();
forward(3);
left(3);pendown();
forward(3);
left(3);pendown();
forward(3);
left(3);pendown();
forward(3);
left(3);pendown();
forward(3);
left(3);pendown();
forward(2);
left(5);pendown();
forward(2);
left(4);pendown();
forward(3);
left(2);pendown();
forward(3);
left(4);pendown();
forward(3);
left(2);pendown();
forward(3);
left(4);pendown();
forward(3);
left(3);pendown();
forward(3);
left(4);pendown();
forward(3);
left(3);pendown();
forward(3);
left(5);pendown();
forward(3);
left(5);pendown();
forward(3);
left(5);pendown();
forward(3);
left(6);pendown();
forward(3);
left(6);pendown();
forward(3);
left(7);pendown();
forward(3);
left(7);pendown();
forward(3);
left(8);pendown();
forward(3);
left(4);pendown();
forward(3);
left(4);pendown();
forward(3);
left(3);pendown();
forward(3);
left(3);pendown();
forward(3);
left(1);pendown();
forward(3);
left(0);pendown();
forward(3);
left(0);pendown();
forward(3);
right(1);pendown();
forward(3);
right(2);pendown();
forward(3);
right(3);pendown();
forward(3);
right(3);pendown();
forward(3);
right(5);pendown();
forward(3);
right(4);pendown();
forward(2);
right(3);pendown();
forward(2);
right(4);pendown();
forward(2);
right(3);pendown();
forward(2);
right(3);pendown();
forward(2);
right(3);pendown();
forward(2);
right(5);pendown();
forward(2);
right(3);pendown();
forward(2);
left(5);pendown();
forward(2);
right(8);pendown();
forward(2);
right(7);pendown();
forward(1);
right(10);pendown();
forward(1);
right(10);pendown();
forward(1);
penup();right(12);pendown();
forward(1);
penup();right(12);pendown();
forward(1);
penup();right(12);pendown();
forward(1);
penup();right(13);pendown();
forward(1);
penup();right(14);pendown();
forward(1);
penup();right(11);pendown();
forward(1);
penup();right(12);pendown();
forward(1);
penup();right(11);pendown();
forward(1);
right(10);pendown();
forward(1);
right(8);pendown();
forward(1);
right(8);pendown();
forward(2);
left(5);pendown();
forward(5);
right(3);pendown();
forward(5);
right(1);pendown();
forward(6);
left(1);pendown();
forward(6);
left(2);pendown();
forward(3);
left(3);pendown();
forward(3);
left(2);pendown();
forward(3);
left(4);pendown();
forward(3);
left(3);pendown();
forward(3);
left(5);pendown();
forward(3);
left(4);pendown();
forward(3);
left(6);pendown();
forward(3);
penup();right(43);pendown();
forward(4);
right(2);pendown();
forward(4);
right(1);pendown();
forward(4);
right(2);pendown();
forward(4);
right(2);pendown();
forward(4);
right(3);pendown();
forward(4);
right(2);pendown();
forward(4);
right(3);pendown();
forward(4);
right(3);pendown();
forward(4);
right(3);pendown();
forward(4);
right(4);pendown();
forward(4);
right(4);pendown();
forward(4);
right(3);pendown();
forward(4);
right(5);pendown();
forward(4);
right(5);pendown();
forward(4);
right(4);pendown();
forward(4);
right(9);pendown();
forward(3);
right(4);pendown();
forward(3);
right(3);pendown();
forward(3);
right(4);pendown();
forward(3);
right(4);pendown();
forward(3);
right(4);pendown();
forward(3);
right(4);pendown();
forward(3);
right(4);pendown();
forward(3);
right(3);pendown();
forward(3);
right(4);pendown();
forward(3);
right(5);pendown();
forward(3);
right(4);pendown();
forward(3);
right(5);pendown();
forward(3);
right(4);pendown();
forward(3);
right(5);pendown();
forward(3);
right(5);pendown();
forward(3);
right(7);pendown();
forward(2);
right(5);pendown();
forward(2);
right(5);pendown();
forward(2);
right(5);pendown();
forward(2);
right(5);pendown();
forward(2);
right(4);pendown();
forward(2);
right(5);pendown();
forward(2);
right(4);pendown();
forward(2);
right(3);pendown();
forward(2);
right(4);pendown();
forward(2);
right(3);pendown();
forward(2);
right(4);pendown();
forward(2);
right(3);pendown();
forward(2);
right(3);pendown();
forward(2);
right(3);pendown();
forward(2);
right(3);pendown();
forward(2);
penup();left(29);penup();
forward(189);
penup();left(24);pendown();
forward(7);
left(2);pendown();
forward(7);
left(1);pendown();
forward(7);
left(1);pendown();
forward(7);
left(1);pendown();
forward(7);
right(0);pendown();
forward(7);
right(0);pendown();
forward(7);
right(0);pendown();
forward(7);
right(1);pendown();
forward(7);
right(1);pendown();
forward(7);
right(1);pendown();
forward(7);
right(2);pendown();
forward(7);
right(2);pendown();
forward(7);
right(2);pendown();
forward(7);
right(2);pendown();
forward(4);
right(1);pendown();
forward(4);
right(2);pendown();
forward(4);
right(1);pendown();
forward(4);
penup();right(40);pendown();
forward(6);
left(1);pendown();
forward(6);
left(1);pendown();
forward(6);
left(0);pendown();
forward(6);
left(1);pendown();
forward(6);
left(0);pendown();
forward(6);
left(0);pendown();
forward(6);
left(0);pendown();
forward(6);
right(1);pendown();
forward(6);
left(0);pendown();
forward(6);
right(1);pendown();
forward(6);
right(1);pendown();
forward(6);
right(1);pendown();
forward(6);
right(2);pendown();
forward(6);
right(1);pendown();
forward(6);
right(2);pendown();
forward(6);
penup();right(116);pendown();
forward(15);
left(2);pendown();
forward(15);
left(1);pendown();
forward(15);
left(0);pendown();
forward(15);
left(0);pendown();
forward(8);
left(0);pendown();
forward(8);
left(0);pendown();
forward(8);
left(0);pendown();
forward(8);
left(0);pendown();
forward(15);
right(1);pendown();
forward(15);
right(5);pendown();
forward(20);
left(1);pendown();
forward(20);
left(0);pendown();
forward(20);
right(1);pendown();
forward(20);
right(0);pendown();
forward(10);
right(1);pendown();
forward(10);
right(1);pendown();
forward(10);
right(0);pendown();
forward(10);
right(2);pendown();
forward(10);
right(1);pendown();
forward(10);
right(1);pendown();
forward(10);
right(2);pendown();
forward(10);
left(4);pendown();
forward(19);
left(2);pendown();
forward(19);
right(0);pendown();
forward(19);
left(1);pendown();
forward(19);
right(0);pendown();
forward(19);
right(0);pendown();
forward(19);
right(1);pendown();
forward(19);
right(0);pendown();
forward(19);
penup();right(25);pendown();
forward(1);
right(0);pendown();
forward(1);

  
}
