var loop = "";
var penPos = "";
var x, y, oldx, oldy, c, alpha, omega;
												//old beta , // pivot
var path  = __dirname;
var ratio = 39.9; //convertion of plt to cm

//npm install prompt
var prompt = require('prompt');

prompt.start();
prompt.get(['filename'], function (err, result) {
    // 
    // Log the results. 
    // 
    var n = result.filename;
    grindingData(n);
});

// var name = "dessin";
// grindingData(name);



function grindingData(fn){
	var fs = require('fs');
	var contents = fs.readFileSync(path+'/'+fn+'.plt', 'utf8');
	var hpgl = contents;
		
	var actions = hpgl.split(";");

	//get rid of IN and empty actions
	var i = actions.indexOf("IN");
	if (i > -1) {
		actions.splice(i, 1);
	}
	i = actions.indexOf("");
	if (i > -1) {
		actions.splice(i, 1);
	}
	//insert PU 00
	actions.splice(0, 0, "PU0,0");
	for(var j = 0; j < actions.length; j++){
		var pen = actions[j].substring(0,2);
		if (pen === 'PU'){
			penPos= "penup();\n"
		}else{
			penPos= "pendown();\n"
		}
		var coord = actions[j].substring(2,actions[j].length);
		coord = coord.split(",");

		//get rid of blank
		if(coord.length === 2 ){
			x = coord[0]/ratio;
			y = coord[1]/ratio;
			c = dist(x, y, oldx, oldy);
			roboturn = angle(x, y, oldx, oldy);
			if (!isNaN(c)){
				if(Math.abs(omega) > 10){
				loop += "penup();"+roboturn+penPos+"forward("+c+");\n";
				}else{
					loop +=  roboturn+penPos+"forward("+c+");\n";
				}
			}

			oldx = x;
			oldy = y;
		}

	}
	console.log(loop);


	fs.writeFile(path+'/'+fn+'.txt', loop, function(err) {
		if(err) {
			return console.log(err);
		}
		console.log('file save as '+fn+'.txt');
	});
}


function angle(x1, y1, x2, y2){
	var t = "";
	Math.degrees = function(radians) {
		return radians * 180 / (Math.PI);
	};
	//console.log(x1,y1);
	var l = (x1-x2);
	var m = (y1-y2);
	var beta = Math.atan2(m,l);
	beta = Math.round(Math.degrees(beta))	;
	console.log(beta);
	if(typeof alpha === 'undefined' && typeof x2 === "undefined"){
		var l = (x1);
		var m = (y1);
		beta = Math.atan2(m,l);
		beta = Math.round(Math.degrees(beta))	;
		//t += "left("+Math.abs(betS)+");";
		t += "left("+beta+");";

	}else if(typeof alpha !== 'undefined'){

		if(beta >= 0 && alpha >= 0){
			omega = Math.abs(alpha) - Math.abs(beta);
			if(omega < 0){
				t += "left("+Math.abs(omega)+");";
			}else if(omega >= 0){
				t += "right("+Math.abs(omega)+");";
			}
		}
		if(beta < 0 && alpha >= 0){
			if( Math.abs(beta) >= 90 ){
				omega = 360-(Math.abs(alpha) + Math.abs(beta));
				t += "left("+Math.abs(omega)+");";
			}else if(Math.abs(beta) < 90){
				omega = Math.abs(alpha) + Math.abs(beta);
				t += "right("+Math.abs(omega)+");";
			}

		}
		if(beta >= 0 && alpha < 0){
			if( Math.abs(beta) >= 90 ){
				omega = 360-(Math.abs(alpha) + Math.abs(beta));
				t += "right("+Math.abs(omega)+");";
			}else if( Math.abs(beta) < 90){
				omega = Math.abs(alpha) + Math.abs(beta);
				t += "left("+Math.abs(omega)+");";
			}
			
		}
		if(beta < 0 && alpha < 0){
			omega = Math.abs(alpha) - Math.abs(beta);
			if(omega < 0){
				t += "right("+Math.abs(omega)+");";
			}else if(omega >= 0){
				t += "left("+Math.abs(omega)+");";
			}
		}
	}

	alpha = beta;

	//console.log(t);
	return t;
}

function dist(x1, y1, x2, y2){
	var l = (x1-x2);
	var m = (y1-y2);
	var d = Math.sqrt(Math.pow(l,2)+Math.pow(m,2)); 
	return  Math.round(d);
}

